# Reading a binary tree from a file #

![Screenshot_1.png](https://bitbucket.org/repo/RzrdKy/images/1076015925-Screenshot_1.png)

We have an unordered/unsorted binary tree where each node is labeled with a word. A word is combination of uppercase and lowercase English letters. There are no duplicate nodes in the tree, all nodes have distinct labels.

The tree is represented in the file as multiple lines, each line corresponds to a node and its direct children and written as:
Parent,Left,Right

If a node does not have a left or right child, we will put # character instead of the child. The lines in the file can appear in random order.

The tree represented by the sample image at the top of the page is written to a file as:
A,Quick,Brown
Quick,Fox,Jumps
Jumps,Dog,#
Brown,#,Over
Fox,The,Lazy

Using C# and standard .Net libraries please complete the following:

Define a Tree class to represent this binary tree
Implement a function with the following definition: public static Tree BuildTree(StreamReader reader). The function will parse the input stream and will return a tree represented by this stream. If the input stream does not represent a valid tree, the function should throw an exception.
Implement unit tests for the BuildTree function.