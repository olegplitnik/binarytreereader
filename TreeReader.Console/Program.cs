﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeReader.Console
{
	class Program
	{
		static void Main(string[] args)
		{
			using (FileStream fs = File.OpenRead(args[0]))
			using (StreamReader sr = new StreamReader(fs))
			{
				var tree = Tree.BuildTree(sr);
			}
		}
	}
}
