﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Text;

namespace TreeReader.Tests
{
	[TestClass]
	public class TreeTests
	{
		private void AssertSampleTree(Tree tree)
		{
			Assert.IsNotNull(tree);
			Assert.IsNull(tree.Parent);
			Assert.IsNotNull(tree.Left);
			Assert.IsNotNull(tree.Right);
			Assert.AreEqual(tree.Right.Value, "Brown");
			Assert.AreEqual(tree.Left.Value, "Quick");
			Assert.AreEqual(tree.Left.Parent, tree);
			Assert.AreEqual(tree.Right.Right.Value, "Over");
			Assert.IsNull(tree.Right.Left);
			Assert.AreEqual(tree.Right.Parent, tree);

			Assert.AreEqual(tree.Left.Left.Value, "Fox");
			Assert.AreEqual(tree.Left.Left.Parent, tree.Left);
			Assert.AreEqual(tree.Left.Right.Value, "Jumps");
			Assert.AreEqual(tree.Left.Right.Left.Value, "Dog");
			Assert.AreEqual(tree.Left.Right.Left.Parent, tree.Left.Right);		
			Assert.IsNull(tree.Left.Right.Right);

			Assert.AreEqual(tree.Left.Left.Left.Value,"The");
			Assert.AreEqual(tree.Left.Left.Right.Value, "Lazy");
			Assert.AreEqual(tree.Left.Left.Left.Parent, tree.Left.Left);
			Assert.AreEqual(tree.Left.Left.Right.Parent, tree.Left.Left);

		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void BuildTree_EmptyStream_ThrowsException()
		{
			var streamReader = new StreamReader(new MemoryStream());
			Tree.BuildTree(streamReader);
		}


		[TestMethod]
		public void BuildTree_ValidTreeStream_Tree()
		{
			var treeLines = @"A,Quick,Brown
Quick,Fox,Jumps
Jumps,Dog,#
Brown,#,Over
Fox,The,Lazy";

			var streamReader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(treeLines)));
			var tree = Tree.BuildTree(streamReader);

			AssertSampleTree(tree);


		}


		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void BuildTree_ManyRootsInTreeStream_ThrowsException()
		{
			var treeLines = @"A,Quick,Brown
Jumps,Dog,#";

			var streamReader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(treeLines)));
			var tree = Tree.BuildTree(streamReader);

		}



		[TestMethod]
		public void BuildTree_ValidTreeWithRandomOrderInStream_Tree()
		{
			var treeLines = @"Jumps,Dog,#
Brown,#,Over
A,Quick,Brown
Quick,Fox,Jumps
Fox,The,Lazy";

			var streamReader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(treeLines)));
			var tree = Tree.BuildTree(streamReader);

			AssertSampleTree(tree);
		}

		[TestMethod]
		public void BuildTree_ValidTreeWithAnotherRandomOrderInStream_Tree()
		{
			var treeLines = @"Fox,The,Lazy
Jumps,Dog,#
A,Quick,Brown
Quick,Fox,Jumps
Brown,#,Over";

			var streamReader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(treeLines)));
			var tree = Tree.BuildTree(streamReader);

			AssertSampleTree(tree);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void BuildTree_NoRootInStream_ThrowsException()
		{
			var treeLines = @"#,#,#";

			var streamReader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(treeLines)));
			var tree = Tree.BuildTree(streamReader);

		}

	}
}
