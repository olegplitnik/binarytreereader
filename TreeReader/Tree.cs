﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeReader
{
	public class Tree
	{
		public Tree Left { get; set; }

		public Tree Right { get; set; }

		public Tree Parent { get; set; }

		public string Value { get; set; }

		public const string NoChildCharacter = "#";
		public const char NodeSeparatorCharacter = ',';

		public static Tree BuildTree(StreamReader reader)
		{
			if (reader == null)
			{
				throw new ArgumentNullException(nameof(reader));
			}

			var dicLeafs = new Dictionary<string, Tree>();
			var dicParents = new Dictionary<string, Tree>();


			while (!reader.EndOfStream)
			{

				var line = reader.ReadLine();
				var vertexes = line.Split(NodeSeparatorCharacter);

				if (vertexes.Length != 3)
				{
					throw new ArgumentException();
				}

				var parent = vertexes[0];
				var left = vertexes[1];
				var right = vertexes[2];


				if (dicLeafs.ContainsKey(left) || dicLeafs.ContainsKey(right) || dicParents.ContainsKey(parent)
					|| parent == NoChildCharacter)
				{
					throw new ArgumentException();
				}

				var parentNode = dicLeafs.ContainsKey(parent) ? dicLeafs[parent] : new Tree { Value = parent };

				if (left != NoChildCharacter)
				{
					parentNode.Left = dicParents.ContainsKey(left) ? dicParents[left] : new Tree { Value = left };
					parentNode.Left.Parent = parentNode;
					dicLeafs.Add(left, parentNode.Left);
				}

				if (right != NoChildCharacter)
				{
					parentNode.Right = dicParents.ContainsKey(right) ? dicParents[right] : new Tree { Value = right };
					parentNode.Right.Parent = parentNode;
					dicLeafs.Add(right, parentNode.Right);
				}

				dicParents.Add(parent, parentNode);
			}

			var roots = dicParents.Where(x => !dicLeafs.ContainsKey(x.Key));

			if (!roots.Any())
			{
				throw new ArgumentException("No root found");
			}

			if (roots.Count() > 1)
			{
				throw new ArgumentException("Multiple roots found");
			}

			return roots.Single().Value;
		}

	}
}
